# SPDX-FileCopyrightText: 2024 Hoang Nguyen <folliekazetani@protonmail.com>
#
# SPDX-License-Identifier: Apache-2.0

_: {
  projectRootFile = "flake.lock";

  programs = {
    buildifier = {
      enable = true;
      includes = [ "BUILD.bazel" "MODULE.bazel" "*.bzl" ];
    };

    cue.enable = true;

    gofumpt = { enable = true; extra = true; };

    nixpkgs-fmt.enable = true;

    statix.enable = true;
  };
}

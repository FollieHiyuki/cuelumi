// SPDX-FileCopyrightText: 2024 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

package tools

import (
	"strings"

	"tool/exec"
	"tool/http"
)

command: "gen-pulumi-metaschema": {
	gitRoot: exec.Run & {
		cmd: ["git", "rev-parse", "--show-toplevel"]
		stdout: string

		path: strings.TrimSpace(stdout)
	}

	getSchema: http.Get & {
		url: "https://raw.githubusercontent.com/pulumi/pulumi/master/pkg/codegen/schema/pulumi.json"
	}

	importSchema: exec.Run & {
		stdin: getSchema.response.body
		cmd:   "cue import -f -p pulumi -l #Package: -o \(gitRoot.path)/schemata/pulumi/package.cue jsonschema: -"
	}
}

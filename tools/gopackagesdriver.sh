#!/usr/bin/env nix
#! nix shell nixpkgs#bazel_7 --command /bin/sh

# SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
#
# SPDX-License-Identifier: Apache-2.0

exec bazel run -- @rules_go//go/tools/gopackagesdriver "$@"

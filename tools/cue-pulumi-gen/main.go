/*
 *  SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
 *
 *  SPDX-License-Identifier: Apache-2.0
 */

package main

import (
	"log"

	"gitlab.com/folliehiyuki/cuemata/internal/cmd"
)

func main() {
	if err := cmd.NewCUEGenCmd().Execute(); err != nil {
		log.Fatalf("Error: %s", err)
	}
}

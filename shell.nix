# SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
#
# SPDX-License-Identifier: Apache-2.0

let
  lock = builtins.fromJSON (builtins.readFile ./flake.lock);
  src = lock.nodes.flake-compat.locked;

  flake-compat =
    assert src.type == "github";
    fetchTarball {
      url = "https://github.com/${src.owner}/${src.repo}/archive/${src.rev}.tar.gz";
      sha256 = src.narHash;
    };
in
(import flake-compat { src = ./.; }).shellNix

# SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
#
# SPDX-License-Identifier: Apache-2.0

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, flake-utils, nixpkgs, treefmt-nix, ... }:
    let
      inherit (nixpkgs) lib;
      templates = lib.filterAttrs (name: type: type == "directory") (builtins.readDir ./templates);
    in
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          treefmtEval = treefmt-nix.lib.evalModule pkgs ./treefmt.nix;
        in
        {
          # Wrap regularly used packages with `nix run .#<program>`, for convenience
          # NOTE: `cue` can be removed from this list after https://github.com/seh/rules_cue/pull/15
          apps = lib.mapAttrs
            (_: value: flake-utils.lib.mkApp { drv = value; })
            (lib.mapAttrs
              (prog: package: "${lib.getBin package}/bin/${prog}")
              (with pkgs; {
                inherit bazel_7 cue;
                buildozer = bazel-buildtools;
                buildifier = bazel-buildtools;
              }));

          checks = with pkgs; {
            treefmt = treefmtEval.config.build.check self;
            golangci-lint = writeShellApplication {
              name = "golangci-lint";
              runtimeInputs = [ go ];
              text = "${golangci-lint}/bin/golangci-lint run";
            };
            license = writeShellScriptBin "reuse-lint" ''
              ${reuse}/bin/reuse lint
            '';
          };

          formatter = treefmtEval.config.build.wrapper;

          devShells.default = with pkgs; mkShellNoCC {
            name = "cuemata";
            meta.description = "Development shell to work with cuemata project";
            inputsFrom = [ treefmtEval.config.build.devShell ];
            packages = [ bazel_7 nix ];
          };
        }) // {
      templates = lib.mapAttrs
        (dir: _: {
          path = ./templates/${dir};
          description = "${dir} project template using cuemata";
        })
        templates;
    };
}

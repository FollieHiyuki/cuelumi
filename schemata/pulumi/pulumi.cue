// SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

package pulumi

#PulumiYAML: {
	// ==============================
	// Base Pulumi.yaml configuration
	// Ref: https://www.pulumi.com/docs/concepts/projects/project-file/
	// ==============================

	name: string

	// A naive file path regular expression
	// NOTE:
	// * don't allow whitespaces or weird characters
	// * also no root directory /
	_pathRegex: =~#"^([\w\.-]+)?(/[\w\.-]+)+/?$"#

	_#RuntimeOpts:
		{
			name: "yaml"
			options: compiler: string
		} |
		{
			name: "python"
			options: virtualenv: _pathRegex
		} |
		{
			name: "nodejs"
			options: {
				typescript?: bool
				nodeargs:    string
			}
		} |
		{
			name: "go"
			options: buildTarget: _pathRegex
		} |
		{
			name: "dotnet" | "java" | "go"
			options: binary: _pathRegex
		}

	runtime: _#RuntimeOpts | "go" | "python" | "nodejs" | "dotnet" | "java" | *"yaml"

	description?: string

	// Basic types that Pulumi accepts
	_#Type: string | int | bool | [...]

	_#Value: {
		value: _#Type | {...}
	}

	// This is only valid for project property keys
	_#Schema: {
		type:         "string" | "boolean" | "integer" | "array"
		description?: string
		secret?:      bool
		default?:     _#Type
		items?: type: "string" | "boolean" | "integer"
	}

	config?: [string]: _#Type | _#Schema | _#Value

	main?: _pathRegex

	// Requires a relative path to Pulumi.yaml, so we can't reuse _pathRegex here
	stackConfigDir?: =~#"^([\w\.-]+/)+$"#

	backend?: url: =~#"^(https?|s3|azblob|gs|file)://.+$"#

	options?: refresh: "always"

	template?: {
		description?: string
		config: {
			description?: string
			default?:     _#Type
			secret?:      bool
		}
	}

	plugins?: ["providers" | "analyzers" | "languages"]: [{
		name:     string
		path?:    _pathRegex
		version?: string
	}]

	// ==============================
	// `pulumi-yaml` specific keys
	// Ref: https://www.pulumi.com/docs/languages-sdks/yaml/yaml-language-reference/
	// ==============================

	_#Resource: {
		type:             string
		defaultProvider?: bool
		properties?: [string]: _#Expression
		options?: {
			additionalSecretOutputs?: [...string]
			aliases?: [...string]
			customTimeouts?: {
				create?: string
				delete?: string
				update?: string
			}
			deleteBeforeReplace?: bool
			dependsOn?: [..._#Interpolation]
			ignoreChanges?: [...string]
			import?:   string
			parent?:   _#Interpolation
			protect?:  bool
			provider?: _#Interpolation
			providers?: [string]: _#Interpolation
			version?:           string
			pluginDownloadURL?: =~#"^https?://.+$"#
			replaceOnChanges?: [...string]
			retainOnDelete?: bool
		}
		get?: {
			id: string
			state?: [string]: _#Expression
		}
	}

	resources?: [string]: _#Resource

	_#BuiltinFn:
		{"fn::toBase64": string} |
		{"fn::fromBase64": =~#"^(?:[A-Za-z0-9+\\/]{4})*(?:[A-Za-z0-9+\\/]{2}==|[A-Za-z0-9+\\/]{3}=|[A-Za-z0-9+\\/]{4})$"#} |
		{"fn::toJSON": _#NonFnMap} |
		{
			"fn::invoke": {
				function:  string
				arguments: _#NonFnMap
				options?: {
					parent?:            _#Interpolation
					provider?:          _#Interpolation
					version?:           string
					pluginDownloadURL?: =~#"^https?://.+$"#
				}
				return?: string
			}
		} |
		{"fn::join": [string, [...string]]} |
		{"fn::split": [string, string]} |
		{"fn::select": [int, [...]]} |
		{"fn::assetArchive": [string]: _#AssetArchiveFn} |
		_#AssetArchiveFn |
		{"fn::readFile": =~#"^([\w\.\$\{\}-]+)?(/[\w\.\$\{\}-]+)*$"#}

	_#AssetArchiveFn:
		{"fn::stringAsset": string} |
		{["fn::fileAsset" | "fn::fileArchive"]: _pathRegex} |
		{["fn::remoteAsset" | "fn::remoteArchive"]: =~#"^(https?|file)://.*$"#}

	// fn::secret is separated to avoid recursion
	_#SecretFn: {"fn::secret": string | _#BuiltinFn}

	_#Interpolation: =~#"^\$\{[A-Za-z0-9]+\}$"#

	_#NonFnMap: [!~#"^fn::[a-zA-Z0-9]+$"#]: _#Type | {...}

	_#Expression: _#Type | _#NonFnMap | _#BuiltinFn | _#SecretFn

	variables?: [string]: _#Expression

	outputs?: [string]: _#Expression
}

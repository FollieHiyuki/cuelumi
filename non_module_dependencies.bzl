# SPDX-FileCopyrightText: 2024 Hoang Nguyen <folliekazetani@protonmail.com>
#
# SPDX-License-Identifier: Apache-2.0

"""
This module contains non-module dependencies used by the repo
(shamelessly stolen from https://github.com/tweag/nix_bazel_codelab/blob/main/non_module_dependencies.bzl).
"""

load("@rules_nixpkgs_go//:go.bzl", "nixpkgs_go_configure")

def _non_module_dependencies_impl(_ctx):
    nixpkgs_go_configure(
        sdk_name = "go_sdk",
        repository = Label("@nixpkgs"),
        register = False,
    )

non_module_dependencies = module_extension(
    implementation = _non_module_dependencies_impl,
)

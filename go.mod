// SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: CC0-1.0

module gitlab.com/folliehiyuki/cuemata

go 1.22.5

require (
	github.com/goccy/go-json v0.10.2
	github.com/goccy/go-yaml v1.11.2
	github.com/pelletier/go-toml/v2 v2.1.0
	github.com/spf13/cobra v1.8.0
)

require (
	github.com/fatih/color v1.10.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)

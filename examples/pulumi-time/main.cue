// SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
//
// SPDX-License-Identifier: Apache-2.0

package main

import "gitlab.com/folliehiyuki/cuemata/schemata/pulumi"

pulumi.#PulumiYAML & {
	name:        "cuemata-example-time"
	runtime:     "yaml"
	description: "Example Pulumi project using CUE with time provider"

	resources: {
		time: {
			type: "pulumi:providers:time"
			options: version: "v0.0.16"
		}
		rotation: {
			type: "time:Rotating"
			properties: rotationDays: 30
			options: provider:        "${time}"
		}
	}

	outputs: date: "${rotation.hour}"
}

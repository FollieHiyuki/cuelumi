/*
 *  SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
 *
 *  SPDX-License-Identifier: Apache-2.0
 */

package cmd

import "github.com/spf13/cobra"

func NewCUEGenCmd() *cobra.Command {
	return nil
}

func NewSchemaCmd() *cobra.Command {
	return nil
}

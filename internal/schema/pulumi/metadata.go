/*
 *  SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
 *
 *  SPDX-License-Identifier: Apache-2.0
 */

package pulumi

import (
	_ "embed"
)

type PkgInfo struct {
	Type           string `toml:"type"`
	Version        string `toml:"version"`
	RepoSlug       string `toml:"repo_slug"`
	SchemaFilePath string `toml:"schema_file_path"`
	GitURL         string `toml:"git_url,omitempty"`
}

const (
	githubURL = "https://github.com"
	gitlabURL = "https://gitlab.com"
)

//go:embed providers.toml
var providers string

/*
 *  SPDX-FileCopyrightText: 2023 Hoang Nguyen <folliekazetani@protonmail.com>
 *
 *  SPDX-License-Identifier: Apache-2.0
 */

package pulumi

import (
	"github.com/goccy/go-json"
	"github.com/goccy/go-yaml"
	"github.com/pelletier/go-toml/v2"
)
